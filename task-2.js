// Write two ways of creating objects using functions that represent
// the items of the products on the products.jpg on the source directory

// Factory Function
// You code here ...

function fastFood(name,info,price){
    return{
        name:name,
        info: info,
        price: price,
        calculaePrice(){
            console.log('calculating...');
        },
    };
} 

const box1 = fastFood(
    'Макси бокс "Популярный"',
    'Информация: лаваш мясная classic, картофель-фри, пеп...',
    ("35000сум"),
);

const box2 = fastFood(
    'Макси бокс "Ретро"', 
    'Информация: шаурма мясная classic, картофель-фри, пеп...',
    ("30000сум"),
);

const box3 = fastFood(
    'Макси бокс "Традиция"', 
    'Информация: клаб-сендвич с курицей, картофель-фри, пеп...',
    ("29000сум"),
);

const box4 = fastFood(
    'Макси бокс "Тренд"', 
    'Информация: гамбургер, картофель-фри, пеп...',
    ("30000сум"),
);


console.log(box1);
console.log(box2);
console.log(box3);
console.log(box4);


// Constructor Function
// You code here ...

function FastFoodBox(name,info,price){
        this.name = name;
        this.info = info;
        this.price = price;
        this.calculaePrice = function(){
            console.log('calculating...');
        };
    } 

const boxs1 = new FastFoodBox(
    'Макси бокс "Популярный"',
    'Информация: лаваш мясная classic, картофель-фри, пеп...',
    ("35000сум"),
);

const boxs2 = new FastFoodBox(
    'Макси бокс "Ретро"', 
    'Информация: шаурма мясная classic, картофель-фри, пеп...',
    ("30000сум"),
);

const boxs3 = new FastFoodBox(
    'Макси бокс "Традиция"', 
    'Информация: клаб-сендвич с курицей, картофель-фри, пеп...',
    ("29000сум"),
);

const boxs4 = new FastFoodBox(
    'Макси бокс "Тренд"', 
    'Информация: гамбургер, картофель-фри, пеп...',
    ("30000сум"),
);


console.log(typeof boxs1);
console.log(typeof boxs2);
console.log(typeof boxs3);
console.log(typeof boxs4);

