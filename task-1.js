// Convert the switch statement into an object called lookup.
// Use it to look up val and assign the associated string to the result variable.

// ! You should not use case, switch, or if statements

// Setup
function phoneticLookup(val) {
  let result = '';

  // Only change code below this line
  switch (val) {
    case 'alpha':
      result = 'Adams';
      break;
    case 'bravo':
      result = 'Boston';
      break;
    case 'charlie':
      result = 'Chicago';
      break;
    case 'delta':
      result = 'Denver';
      break;
    case 'echo':
      result = 'Easy';
      break;
    case 'foxtrot':
      result = 'Frank';
  }

  // Only change code above this line

  const lookup = {
    'alpha': 'Adams',
    'bravo': 'Boston',
    'charlie': 'Chicago',
    'delta': 'Denver',
    'echo': 'Easy',
    'foxtrot': 'Frank',
  };

  return result[val] = lookup[val] || ''; 
}

console.log(phoneticLookup('charlie')); // Chicago
console.log(phoneticLookup('alpha')); // Adams
console.log(phoneticLookup('bravo')); // Boston
console.log(phoneticLookup('delta')); // Denver
console.log(phoneticLookup('echo')); // Easy
console.log(phoneticLookup('foxtrot')); // Frank
console.log(phoneticLookup(' ')); // undefined
